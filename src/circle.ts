export default class Circle {
    radius: number;

    constructor(radius: number) {
        this.radius = radius;
    }

    getArea() {
        return 2 * Math.PI * this.radius;
    }
}